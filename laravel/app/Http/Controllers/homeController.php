<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mHome;
use Symfony\Component\VarDumper\VarDumper;

class homeController extends Controller
{
    function index()
    {
        //menampilkan data select all 
        //$movie = mHome::all();

        //menampilkan data limit / pagination 
        $movie = mHome::orderBy('id', 'desc')->paginate(5);

        return view('admin.home', compact('movie'));
    }
    function blog($slug)
    {
        //urlSlug berfungsi untuk mendefinisikan nama untuk digunakan pada View
        // die($slug);

        return view('admin.blog', compact('slug'));
        //return view('admin.blog', ['urlSlug' => $slug]);
    }

    function formAddMovie()
    {

        return view('admin.formAddMovie');
    }
    function addMovie(Request $param)
    {
        //dd($param); sama kayak print r

        $param->validate([
            'title' => 'required|max:255',
            'tahun' => 'required',
            'deskripsi' => 'required|min:5',
        ]);

        $add = new mHome;
        $add->title = $param->title;
        $add->year = $param->tahun;
        $add->subject = $param->deskripsi;
        $add->save();

        return redirect('/home');
    }

    function editMovie($id)
    {
        var_dump('sanjay');
        exit;
    }
}
