<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHome extends Model
{
    protected $guarded  = [];
    //tabel ini di panggil dari db satu model datu tabel 
    protected $table    = "tb_movie";
    //untuk menonaktifkan created add and update at 
    public $timestamps = false;
}
