<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'homeController@index');
// Route::get('/blog', 'homeController@blog');
//home/
Route::get('/home/formAddMovie', 'homeController@formAddMovie');
//Route::get('/home/{slug}', 'homeController@blog');
Route::post('/home/addMovie', 'homeController@addMovie');

//belakang itu nama
Route::get('/home/{$id}/editMovie', 'homeController@editMovie');
