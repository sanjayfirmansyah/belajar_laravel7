@extends('template.header')


@section('content')

<!-- ini untuk menampilkan error  -->
<!-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 -->
 <h1>Form Edit</h1>
<form action="{{ url('/home/addMovie') }}" method="post">
	@csrf
	<div class="form-group">
		<label for="exampleFormControlInput1">Title Movie</label>
		<input type="text" class="form-control" name="title" id="title" placeholder="The Sanjay" value="{{old('title')}}">
		@error('title')
		<div class="alert alert-danger">{{ $message }}</div>
		@enderror
	</div>
	<div class="form-group">
		<label >Tahun Rilis</label>
		<select class="form-control" id="tahun" name="tahun">
			<option>2019</option>
			<option>2020</option>
			<option>2021</option>
		</select>
		@error('tahun')
		<div class="alert alert-danger">{{ $message }}</div>
		@enderror
	</div>

	<div class="form-group">
		<label>Deskripsi</label>
		<textarea class="form-control" name="deskripsi" id="deskripsi" rows="3" >{{ old('subject') }}</textarea>
		@error('deskripsi')
		<div class="alert alert-danger">{{ $message }}</div>
		@enderror
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection