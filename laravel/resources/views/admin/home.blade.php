@extends('template.header') <!-- ini reference dengan template  -->

<!-- ini di dalam body -->
@section('content')
	
	{{-- <a href="{{url('/home/formAddMovie')}}" class="btn btn-primary" title="">Add Movie+</a> --}}
		<?php foreach ($movie as $val): ?>
			<div class="card mb-1">
				<div class="card-body">
					
					<strong><p>Judul : <?= $val['title'] ?></p></strong>
					<p>tahun = <?= $val['year'] ?></p>
					<p>deskripsi <?= $val['subject'] ?></p>
					
					<a href="{{ url('') }}/home/{{$val->id}}/editMovie" class="btn btn-secondary">Edit</a>
					<a href="" class="btn btn-danger">Delete</a>
					
				</div>
			</div>
		<?php endforeach ?>


		<div>
			<!-- menampilkan view paginationya  -->
			{{$movie ->links()}}
		</div>

@endsection
<!-- ini di akhir body  -->